import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  signInForm: FormGroup;
  signUp = { email: '', password: '' };
  sendingStatus: string;
  message: string;

  ngOnInit(): void {
    this.signInForm = new FormGroup({
      email: new FormControl(this.signUp.email, [
        Validators.pattern(
          /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
        ),
      ]),
      password: new FormControl(this.signUp.password, [Validators.required]),
    });
  }

  get email(): any {
    return this.signInForm.get('email');
  }
  get password(): any {
    return this.signInForm.get('password');
  }

  constructor() {}

  signIn(): void {
    if (this.signInForm.invalid) {
      return;
    }

    this.sendingStatus = 'pending';
    this.message = 'Please wait...';

    setTimeout(() => {
      this.showResults();
    }, 3000);
  }

  // only for this example
  randomScenario(): IScenario {
    const scenarioArray = [
      // { message: 'Welcome to our App!', status: 'success' },
      {
        message: 'Email address or password are incorrect. Please try again.',
        status: 'error',
      },
      {
        message: 'Sorry, We have technical problem, please try again later.',
        status: 'error',
      },
    ];
    const scenarioNumber = Math.floor(Math.random() * scenarioArray.length);
    return scenarioArray[scenarioNumber];
  }

  showResults(): void {
    const selectedScenario = this.randomScenario();
    this.sendingStatus = selectedScenario.status;
    this.message = selectedScenario.message;

    if (this.sendingStatus === 'error') {
      setTimeout(() => {
        this.signInForm.reset();
        this.sendingStatus = null;
        this.message = null;
      }, 5000);
    }
  }
}

export interface IScenario {
  message: string;
  status: string;
}
