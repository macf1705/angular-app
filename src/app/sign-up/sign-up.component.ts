import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { confirmPasswordValidator } from '../directives/must-match.directive';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  signUp = { email: '', fullName: '', password: '', confirmPassword: '' };
  sendingStatus: string;
  message: string;

  ngOnInit(): void {
    this.signUpForm = new FormGroup(
      {
        email: new FormControl(this.signUp.email, [
          Validators.required,
          Validators.pattern(
            /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
          ),
        ]),
        fullName: new FormControl(this.signUp.fullName, [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(/^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/i),
        ]),
        password: new FormControl(this.signUp.password, [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl(this.signUp.password, [
          Validators.required,
        ]),
      },
      { validators: confirmPasswordValidator }
    );
  }

  get email(): any {
    return this.signUpForm.get('email');
  }
  get fullName(): any {
    return this.signUpForm.get('fullName');
  }
  get password(): any {
    return this.signUpForm.get('password');
  }
  get confirmPassword(): any {
    return this.signUpForm.get('confirmPassword');
  }

  constructor(/*private httpClient: HttpClient*/) {}

  submitForm(): void {
    if (this.signUpForm.invalid) {
      return;
    }

    this.sendingStatus = 'pending';
    this.message = 'Please wait...';

    setTimeout(() => {
      this.showResults();
    }, 3000);

    // in real live
    // const formData: any = new FormData();
    // formData.append('email', this.signUpForm.get('email').value);
    // formData.append('fullName', this.signUpForm.get('fullName').value);
    // formData.append('password', this.signUpForm.get('password').value);
    // formData.append('confirmPassword', this.signUpForm.get('confirmPassword').value);
    // this.httpClient.post<any>('url to server', formData).subscribe(
    //   (res) => {
    //     this.sendingStatus = 'success';
    //     this.message = 'Thank you for register!';
    //     console.log(res);
    //   },
    //   (err) => {
    //     this.sendingStatus = 'error';
    //     this.message = 'Please try again later.';
    //     console.log(err);
    //   }
    // );
  }

  // only for this example
  randomScenario(): IScenario {
    const scenarioArray = [
      { message: 'Thank you for register!', status: 'success' },
      { message: 'Please try again later.', status: 'error' },
    ];
    const scenarioNumber = Math.floor(Math.random() * scenarioArray.length);
    return scenarioArray[scenarioNumber];
  }

  showResults(): void {
    const selectedScenario = this.randomScenario();
    this.sendingStatus = selectedScenario.status;
    this.message = selectedScenario.message;
    console.log(JSON.stringify(this.signUpForm.value, null, 4));
  }
}

export interface IScenario {
  message: string;
  status: string;
}
